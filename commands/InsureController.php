<?php
namespace app\commands;

use app\models\Currency;
use yii\console\Controller;

class InsureController extends Controller
{

    public function actionGet()
    {
        $feed = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp');
        $items = simplexml_load_string($feed);

        foreach ($items->Valute as $value) {
            $id = (string) $value->attributes()->ID;
            if (!$currency = Currency::find()->where(['id' => $id])->one()) {
                $currency = new Currency();
            }
            $currency->id = $id;
            $currency->name = (string) $value->Name;
            $currency->rate = (string) $value->Value;

            echo "<pre>";
            print_r($value->Name);
            print_r($value->Value);
            echo "</pre>";

            $currency->save();
        }
    }

}

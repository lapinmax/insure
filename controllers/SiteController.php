<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Currency;



class SiteController extends Controller
{

    public function actionIndex()
    {
        $currencies = Currency::find()->all();

        return $this->render('currency', ['currencies'=>$currencies]);
    }

}
